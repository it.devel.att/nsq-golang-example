#### NSQ Experiments
[Official nsq documentation](https://nsq.io/overview/design.html)

Open terminal and launch docker-compose
```shell script
docker-compose up
```

For run producer
```shell script
go run cmd/producer/main.go -nsqd-addr 127.0.0.1:4150
```

For run consumer
```shell script
go run cmd/consumer/main.go -nsq-lookupd-addrs 127.0.0.1:4161,127.0.0.1:4261
```

#### We have:
* 2 nsqlookup services on hosts:
```
127.0.0.1:4160
127.0.0.1:4260
```
* 4 nsqd services:
```
127.0.0.1:4150
127.0.0.1:4250
127.0.0.1:4350
127.0.0.1:4450
```
> But the nsqd on 127.0.0.1:4450 broadcast his address only to first nsqlookupd(127.0.0.1:4160)
* 1 nsq admin services:
```
127.0.0.1:4171
```
