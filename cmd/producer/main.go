package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"nsq-example/pkg/message"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/nsqio/go-nsq"
)

type Config struct {
	NSQDAddress string `json:"nsqd_address"`
	Topic       string `json:"topic"`
}

func main() {
	addr := flag.String("nsqd-addr", "127.0.0.1:4150", "Address of nsqd instance for publish message")
	flag.Parse()

	nsqdAddr := *addr
	sysCfg := Config{
		NSQDAddress: nsqdAddr,
		Topic:       "topic",
	}

	config := nsq.NewConfig()
	producer, err := nsq.NewProducer(sysCfg.NSQDAddress, config)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		i := 0
		for {
			time.Sleep(time.Second)
			msg := message.Message{
				Body: strconv.Itoa(i),
			}
			bMsg, err := json.Marshal(msg)
			if err != nil {
				log.Println(err)
				continue
			}
			err = producer.Publish(sysCfg.Topic, bMsg)
			if err != nil {
				log.Println(err)
				continue
			}
			fmt.Printf("Send %#v\n", msg)
			i++
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	<-sigChan
	log.Println("Interrupt signal")
	producer.Stop()
}
