package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/nsqio/go-nsq"

	"nsq-example/pkg/message"
)

type handler struct {
	consumer *nsq.Consumer
}

func (h *handler) Filter(addrs []string) []string {
	// Count of nsqd producer can change during time, and I want to have runtime ability consume messages from each producer
	fmt.Println(addrs)
	h.consumer.ChangeMaxInFlight(len(addrs))
	return addrs
}

func (*handler) HandleMessage(m *nsq.Message) error {
	msg := message.Message{}
	if err := json.Unmarshal(m.Body, &msg); err != nil {
		return err
	}
	// TODO For check retries
	//if rand.Intn(3) == 0 {
	//	fmt.Printf("Random err: %#v\n", msg)
	//	return fmt.Errorf("random err")
	//}
	fmt.Printf("[%v] Success message: %#v| Attempts: %v\n", m.NSQDAddress, msg, m.Attempts)
	return nil
}

type Config struct {
	NSQDAddr        string   `json:"nsqd_addr"`
	NSQLookupdAddrs []string `json:"nsq_lookupd_addrs"`
	Topic           string   `json:"topic"`
	Channel         string   `json:"channel"`
}

func main() {
	nsqLookupdAddrs := flag.String("nsq-lookupd-addrs", "127.0.0.1:4161", "Addresses of nsqlookupd instances for discover nsqd instances")
	flag.Parse()
	sysCfg := Config{
		NSQDAddr:        "",
		NSQLookupdAddrs: strings.Split(*nsqLookupdAddrs, ","),
		Topic:           "topic",
		Channel:         "channel",
	}

	config := nsq.NewConfig()
	if err := config.Set("lookupd_poll_interval", time.Second*15); err != nil {
		log.Fatal(err)
	}
	consumer, err := nsq.NewConsumer(sysCfg.Topic, sysCfg.Channel, config)
	if err != nil {
		log.Fatal(err)
	}
	h := &handler{consumer: consumer}
	consumer.SetBehaviorDelegate(h)
	consumer.AddHandler(h)
	if err := consumer.ConnectToNSQLookupds(sysCfg.NSQLookupdAddrs); err != nil {
		log.Fatal(err)
	}

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	<-sigChan
	log.Println("Interrupt signal")
	consumer.Stop()
}
